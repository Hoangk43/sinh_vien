<%@page import="bean.sinhvienbean"%>
<%@page import="bo.sinhvienbo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="bean.monhocbean"%>
<%@page import="bo.monhocbo"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Trang chủ</a></li>
      
      <li><a href="#">Thông tin</a></li>
      <li><a href="#">Hỗ trợ hoạt động học tập</a></li>
      <li><a href="#">Sinh viên</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>
  
<table width="1000" align="center">
   <tr>
      <td width="200" valign="top">
      <table class="table table-hover">
      <%monhocbo mbo =new monhocbo();
        ArrayList<monhocbean> dsmon = mbo.getmon();
        for(monhocbean m: dsmon){
      %>
          <tr>
           <td>
           <a href="hienthi.jsp?mm=<%=m.getMamon()%>"> 
           	<%=m.getTenmon()%>
           	</a> 
          </td>
          </tr>
          <%} %>
       </table>
        </td>
      <td width="600" valign="top">
      <table class="table table-hover">
      <%
      request.setCharacterEncoding("utf-8");
      response.setCharacterEncoding("utf-8");
      
      	sinhvienbo sbo = new sinhvienbo();
        ArrayList<sinhvienbean> dssv = sbo.getsinhvien();
        String mm = request.getParameter("mm");
        String key = request.getParameter("txttk");
        if(mm!=null)
        	dssv = sbo.TimMa(mm);
        else if(key != null) {
        	dssv = sbo.Tim(key);	
        }
        int n = dssv.size();
        for(int i = 0; i < n ; i++){
        	sinhvienbean s = dssv.get(i);
        	%>
         <tr>
         <td>
            <img src="<%=s.getAnh() %>" width="250" height="300"> <br><br>
            Họ tên: <%=s.getHoten() %> <br>
            Địa chỉ: <%=s.getDiachi() %> <br>
            Email: <%=s.getEmail() %> <br>
            Mã sinh viên: <%=s.getMasinhvien() %> <br>
         </td>
         <%i++;
         if(i<n){
         s = dssv.get(i);%>
         <td>
            <img src="<%=s.getAnh() %>" width="250" height="300"> <br><br>
            Họ tên: <%=s.getHoten() %> <br>
            Địa chỉ: <%=s.getDiachi() %> <br>
            Email: <%=s.getEmail() %> <br>
            Mã sinh viên: <%=s.getMasinhvien() %> <br>
         </td>
         <%} %>
         </tr>
       <%} %>
       <%if(n==0){ %>
        <h3>Tìm không thấy sinh viên</h3>
        <%} %>
       </table>
       </td>
      <td width="200" valign="top">
      <form action="hienthi.jsp" method="post">
 		 <input class="form-control" name="txttk" type="text" value="" placeholder="Nhap thông tin tìm kiếm"> <br>
  		 <input class="btn-primary" name="butt" type="submit" value="Search">
		</form>
       
       </td>
   </tr>

</table>
</body>
</html>


