package bo;

import java.util.ArrayList;

import bean.monhocbean;
import bean.sinhvienbean;
import dao.monhocdao;
import dao.sinhviendao;

public class sinhvienbo {
	monhocdao mhoc = new monhocdao();
	ArrayList<monhocbean> ds;
	public ArrayList<monhocbean> getmon(){
		ds = mhoc.getmon();
		return ds;
	}
	
	sinhviendao svdao = new sinhviendao();
	ArrayList<sinhvienbean> dssv;
	public ArrayList<sinhvienbean> getsinhvien(){
		dssv = svdao.getsinhvien();
		return dssv;
	}
	
	public ArrayList<sinhvienbean> TimMa(String mamon){
		ArrayList<sinhvienbean> tam = new ArrayList<sinhvienbean>();
		for(sinhvienbean sv : dssv)
			if(sv.getMamon().equals(mamon))
				tam.add(sv);
		return tam;
	}

	public ArrayList<sinhvienbean> Tim(String key){
		ArrayList<sinhvienbean> tam = new ArrayList<sinhvienbean>();
		for(sinhvienbean sv : dssv)
			if(sv.getHoten().toLowerCase().contains(key.toLowerCase())||	
					sv.getDiachi().toLowerCase().contains(key.toLowerCase())||
					sv.getMamon().toLowerCase().contains(key.toLowerCase())
					)
				tam.add(sv);
		return tam;
	}	
}
