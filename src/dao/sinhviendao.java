package dao;

import java.util.ArrayList;

import bean.sinhvienbean;

public class sinhviendao {
	public ArrayList<sinhvienbean> getsinhvien(){
		ArrayList<sinhvienbean> ds = new ArrayList<sinhvienbean>();
		ds.add(new sinhvienbean("sv1","Nguyễn Thế Anh","1 Nguyễn Trãi","ntanh@gmail.com","a1.jpg","1"));
		ds.add(new sinhvienbean("sv2","Châu Hoàng Bích Du","2 Nguyễn Huệ","chbdu@gmail.com","a2.jpg","2"));
		ds.add(new sinhvienbean("sv3","Trần Hữu Hoàng Giang","3 Nguyễn Trãi","thhgiang@gmail.com","a3.jpg","3"));
		ds.add(new sinhvienbean("sv4","Nguyễn Minh Hiếu","4 Nguyễn Huệ","nmhue@gmail.com","a4.jpg","3"));
		ds.add(new sinhvienbean("sv5","Huỳnh Tấn Khiết","5 Nguyễn Trãi","htkhiet@gmail.com","a5.jpg","2"));
		ds.add(new sinhvienbean("sv6","Lê Dương Bảo Lâm","6 Nguyễn Huệ","ldblam@gmail.com","a6.jpg","1"));		
		return ds;
	}
}
